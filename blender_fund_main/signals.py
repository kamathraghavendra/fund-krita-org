import logging
import typing

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
import django.db.models.signals as django_signals

from blender_id_oauth_client import signals as bid_signals

import looper.signals
import looper.models
from . import models

log = logging.getLogger(__name__)

# The signal's sender is the membership itself.
membership_activated = django_signals.Signal(providing_args=['old_status'])
membership_deactivated = django_signals.Signal(providing_args=['old_status'])
membership_cancelled = django_signals.Signal(providing_args=[])
# Sent when a membership is created in an inactive state because it still needs payment.
# This happens when the payment method doesn't support transactions/charges (e.g. bank).
membership_created_needs_payment = django_signals.Signal(providing_args=[])


@receiver(django_signals.post_save, sender=looper.models.Subscription)
def handle_created_subscription(sender, instance: looper.models.Subscription, **kwargs):
    """Create a new Membership associated with the Subscription.

    Note that this does not yet activate the membership. This is done
    in response to the `looper.signals.subscription_activated signal`.
    """
    if not kwargs.get('created'):
        return

    subscription = instance
    if hasattr(subscription, 'membership'):
        log.info('Not creating membership for user %r subscription %r; membership already attached',
                 subscription.user, subscription)
        return

    # TODO handle case when there is no matching MembershipLevel and Plan Name
    # Now we have a try except because when running tests in Looper we do not create
    # MembershipLevel. In the blender_fund project this should never fail.
    try:
        membership_level = models.MembershipLevel.objects.get(plan=subscription.plan)
    except models.MembershipLevel.DoesNotExist:
        log.warning('MembershipLevel %r does not exist' % subscription.plan.name)
        return

    log.info('Creating membership for user %r subscription %r',
             subscription.user, subscription)
    m = models.Membership(
        user=subscription.user,
        display_name=subscription.user.customer.full_name,
        level=membership_level,
        subscription=subscription,
    )
    m.save()

    payment_method = subscription.payment_method
    if not payment_method:
        return
    if not payment_method.gateway.provider.supports_transactions:
        # Without an action from the user, this membership is never going to get paid.
        membership_created_needs_payment.send(sender=m)


@receiver(django_signals.post_save, sender=looper.models.Subscription)
def handle_managed_flag(sender, instance: looper.models.Subscription, **kwargs):
    membership: typing.Optional[models.Membership] = getattr(instance, 'membership', None)
    if not membership:
        return

    is_managed = instance.collection_method == 'managed'
    if membership.is_managed == is_managed:
        return

    membership.is_managed = is_managed
    if membership.pk:
        log.debug('Updating is_managed flag for membership %r due to save of subscription %r',
                  membership.pk, instance.pk)
        membership.save(update_fields={'is_managed'})


@receiver(looper.signals.subscription_activated)
def handle_activated_subscription(sender: looper.models.Subscription, **kwargs):
    """Activate Membership associated with the activated Subscription."""

    # Iterate over each Membership explicitly to ensure that the appropriate
    # save() methods are called and signals are sent.
    for membership in models.Membership.objects.filter(subscription=sender):
        membership.status = 'active'
        membership.save()


@receiver(looper.signals.subscription_deactivated)
def handle_deactivated_subscription(sender: looper.models.Subscription, **kwargs):
    """Deactivate Membership associated with the Subscription."""

    # Iterate over each Membership explicitly to ensure that the appropriate
    # save() methods are called and signals are sent.
    for membership in models.Membership.objects.filter(subscription=sender):
        membership.status = 'inactive'
        membership.save()


# TODO(Sybren): maybe just move this to the save() function?
@receiver(django_signals.post_save, sender=models.Membership)
def grant_badge_for_membership(sender, instance: models.Membership, **kwargs):
    """Grant a badge to the owner of this membership."""
    from . import badges

    my_log = log.getChild('grant_badge_for_membership')

    if instance.user_id == settings.LOOPER_SYSTEM_USER_ID:
        # The system user doesn't exist in Blender ID, so there is no use to
        # try and grant a badge anyway.
        my_log.info('Not handling badge assignment for Looper system user (uid %r)',
                    instance.user_id)
        return

    badge_to_grant = instance.bid_badge
    current_badge = instance.granted_badge or ''
    if current_badge == badge_to_grant:
        # If the correct badge was already granted, don't bother updating now.
        return

    # Before revoking a badge, check that there are no other memberships that
    # grant this badge.
    other_granters_count: int = instance.user.memberships \
        .filter(granted_badge=current_badge) \
        .exclude(pk=instance.pk) \
        .count()
    badge_to_revoke = current_badge if other_granters_count == 0 else ''

    if badge_to_grant or badge_to_revoke:
        my_log.info(
            'Updating badge on user %r due to their fund membership level %s (grant=%r revoke=%r)',
            instance.user.pk, instance.level, badge_to_grant, badge_to_revoke)
        badges.change_badge(
            user=instance.user,
            revoke=badge_to_revoke,
            grant=badge_to_grant)

    instance.granted_badge = badge_to_grant
    instance.save(update_fields={'granted_badge'})


# TODO(Sybren): maybe just move this to the delete() function?
@receiver(django_signals.post_delete, sender=models.Membership)
def revoke_badge_for_membership(sender, instance: models.Membership, **kwargs):
    """Revoke a badge from the owner of this membership."""
    from . import badges

    my_log = log.getChild('grant_badge_for_membership')

    if instance.user_id == settings.LOOPER_SYSTEM_USER_ID:
        # The system user doesn't exist in Blender ID, so there is no use to
        # try and grant a badge anyway.
        my_log.info('Not handling badge assignment for Looper system user (uid %r)',
                    instance.user_id)
        return

    current_badge = instance.granted_badge or ''
    if not current_badge:
        return

    other_granters_count: int = instance.user.memberships \
        .filter(granted_badge=current_badge) \
        .exclude(pk=instance.pk) \
        .count()
    if other_granters_count:
        return

    my_log.info('Revoking badge %r from user %r due to loss of their fund membership %s',
                current_badge, instance.user.pk, instance)
    badges.change_badge(user=instance.user, revoke=current_badge)


@receiver(bid_signals.user_created)
def set_customer_fullname(sender, instance: User, oauth_info: dict, **kwargs):
    """Create a Customer when a new User is created via OAuth."""
    my_log = log.getChild('set_customer_fullname')
    if not instance.customer:
        my_log.info('NOT updating user full_name of user %s, as they have no Customer')
        return

    my_log.info('Updating Customer full_name as result of OAuth login of %s', instance.pk)
    instance.customer.full_name = oauth_info['full_name']
    instance.customer.save()


@receiver(post_save, sender=User)
def create_customer(sender, instance: User, created, **kwargs):
    from looper.models import Customer

    if not created:
        return

    my_log = log.getChild('create_customer')
    try:
        customer = instance.customer
    except Customer.DoesNotExist:
        pass
    else:
        my_log.debug(
            'Newly created User %d already has a Customer %d, not creating new one',
            instance.pk,
            customer.pk,
        )
        return

    my_log.info('Creating new Customer due to creation of user %s', instance.pk)
    Customer.objects.create(user=instance, billing_email=instance.email)

# TODO(Sybren): respond to M2M change on Membership, when assigning membership to another user.
