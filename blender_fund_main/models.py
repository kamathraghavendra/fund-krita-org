import logging
import typing

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

import blender_id_oauth_client.models
import looper.model_mixins
import looper.exceptions
from looper.models import Subscription, Plan
from looper.money import Money

log = logging.getLogger(__name__)


class MembershipLevel(looper.model_mixins.RecordModifcationMixin, models.Model):
    # There seems to be no integrated way to create a multiselect with arbirary values, so we store
    # them here in a list.
    # TODO validate visible_attributes on save agains the list and provide helpful error message
    # TODO(Sybren): ensure that this list always matches properties of the Membership and
    # MembershipForm classes.
    ALLOWED_VISIBLE_ATTRIBUTES = ['display_name', 'description', 'url', 'logo']

    CATEGORIES = [
        ('INDIV', 'Individual'),
        ('CORP', 'Corporate'),
    ]

    record_modification_fields = {'bid_badge_name', 'is_visible'}

    category = models.CharField(max_length=6, choices=CATEGORIES)
    name = models.CharField(max_length=250)
    # Visible attributes are properties of the Membership model (name, description, url, logo)
    visible_attributes = models.CharField(
        max_length=250, null=False, blank=True, default='',
        help_text=f'Comma separated attributes. Spaces will be stripped. '
                  f'Choose from {", ".join(ALLOWED_VISIBLE_ATTRIBUTES)}')
    is_visible = models.BooleanField(
        editable=False, default=False,
        help_text='Whether memberships of this level have any visible attributes.')
    details_html = models.TextField(null=True, blank=True)
    image = models.ImageField(
        upload_to='membershiplevels/', null=True, blank=True,
        help_text='Image shown on the front page')
    bid_badge_name = models.CharField(
        max_length=80, null=False, default='', blank=True,
        help_text='Krita ID badge associated with this membership level. Note that MyKrita  '
                  'must be set up to allow management of this badge. Also note that changing this '
                  'badge name does not adjust the badges of already-existing membership holders.')
    # Optionally sort membership levels (used in landing page)
    order = models.PositiveIntegerField(null=True, blank=True)
    plan = models.OneToOneField(Plan, on_delete=models.CASCADE, null=True, blank=True)
    is_popular = models.BooleanField(default=False,
                                     help_text='Show as "popular" on the landing page.')

    class Meta:
        ordering = ['order', 'name']

    @property
    def visible_attributes_set(self) -> typing.Set[str]:
        if not self.visible_attributes:
            return set()
        return set(self._visible_attributes)

    @property
    def visible_attributes_list(self) -> typing.List[str]:
        if not self.visible_attributes:
            return []
        return list(self._visible_attributes)

    @property
    def _visible_attributes(self) -> typing.Iterable[str]:
        if not self.visible_attributes:
            return ()
        return (attr.strip() for attr in self.visible_attributes.split(','))

    @property
    def hidden_attributes_set(self) -> typing.Set[str]:
        """In some cases we want to hide form fields."""
        return set(self.ALLOWED_VISIBLE_ATTRIBUTES) - self.visible_attributes_set

    def __str__(self) -> str:
        return self.name

    def save(self, *args, update_fields: typing.Optional[set] = None, **kwargs) -> None:
        is_new: bool = not self.pk
        self.is_visible = bool(self.visible_attributes)

        was_changed, old_state = self.pre_save_record()

        if was_changed and update_fields is not None and old_state['is_visible'] != self.is_visible:
            update_fields.add('is_visible')

        super().save(*args, update_fields=update_fields, **kwargs)

        if not was_changed or is_new:
            return
        self._refresh_bid_badges(old_state)

    def _refresh_bid_badges(self, old_state: looper.model_mixins.OldStateType) -> None:
        """Detect a change in BID badge name, and reassign badges."""

        old_bid_badge_name: str = old_state.get('bid_badge_name', '')
        if old_bid_badge_name == self.bid_badge_name:
            return

        log.info('MembershipLevel %r pk=%d change BID badge name from %r to %r, '
                 'updating memberships', self.name, self.pk, old_bid_badge_name,
                 self.bid_badge_name)
        for membership in self.membership_set.exclude(granted_badge=self.bid_badge_name):
            # Just save the membership, it's enough to trigger the post-save
            # signal that refreshes the badges.
            membership.save()

    def price_per_month(self, currency: str) -> typing.Optional[Money]:
        """Compute the price per month for this membership level.

        It is assumed that all payment plan variations for this level
        represent the same amount of money per unit of time, so a random
        one of the correct currency is chosen.

        Note that this is a lossy operation, as it rounds down to entire cents.

        :return: the price per month, or None if there is no payment plan
            variation for the requested currency.
        """
        planvar = self.plan.variation_for_currency(currency)
        if planvar is None:
            return None
        return planvar.price_per_month


class MembershipManager(models.Manager):

    def active(self) -> models.QuerySet:
        """Return QuerySet of only the active memberships."""
        return self.filter(status='active')

    def visible(self) -> models.QuerySet:
        """Return QuerySet of only the publicly visible memberships."""
        return self.active().filter(level__is_visible=True, is_private=False)


# Ignoring type because of the inner Meta class of the two mix-ins.
class Membership(looper.model_mixins.RecordModifcationMixin,  # type: ignore
                 looper.model_mixins.CreatedUpdatedMixin,
                 models.Model):
    """A Development Fund Membership.

    Membership can be managed manually, or in relation to a Subscription. Depending
    on the membership level, users can edit different sets of attributes. In the
    current implementation:

    - Main:      Name, URL, Logo, Description
    - Diamond:   Name, URL, Logo
    - Titanium:  Name, URL
    - Platinum:  Name, URL
    - Gold:      Name

    """
    record_modification_fields = {'status', 'granted_badge', 'user_id'}
    log = log.getChild('Membership')

    STATUS_DEFAULT = 'inactive'
    STATUSES = (
        'active',
        # Note that the 'inactive' state may require inspection of the subscription,
        # as 'inactive' can mean:
        #   - 'waiting for payment' (subscription status 'on-hold'), or
        #   - 'cancelled' (subscription status 'cancelled').
        'inactive',
    )
    STATUS_CHOICES = tuple((status, status.title()) for status in STATUSES)

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='memberships')
    subscription = models.OneToOneField(Subscription, on_delete=models.SET_NULL,
                                        null=True, blank=True)
    display_name = models.CharField(
        max_length=255,
        null=True, blank=True,
        help_text='Name to show on the front page, if this membership is shown on the front page.')
    description = models.TextField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    logo = models.ImageField(upload_to='logo', null=True, blank=True)
    status = models.CharField(choices=STATUS_CHOICES, default=STATUS_DEFAULT, max_length=20)
    is_private = models.BooleanField(default=False)  # Hide Membership to the world
    level = models.ForeignKey(MembershipLevel, on_delete=models.CASCADE)

    granted_badge = models.CharField(
        max_length=80, default='', blank=True, editable=False,
        help_text='The granted MyKDE badge for this membership, if any was granted yet.'
    )
    is_managed = models.BooleanField(
        default=False, editable=False,
        help_text="Whether this membership's subscription is a managed subscription")

    objects = MembershipManager()

    def __str__(self):
        return f"{self.level.name} - {self.display_name!r}"

    @property
    def bid_badge(self) -> str:
        """MyKDE badge that should be granted for this Membership.

        Takes the status of the membership into account.

        :return: the badge name, or an empty string if no badge should be granted.
        """
        if self.status == 'active' and not self.is_private:
            return self.level.bid_badge_name or ''
        return ''

    def save(self, *args, **kwargs) -> None:
        # Late import because 'signals' imports this module.
        from . import signals

        was_changed, old_state = self.pre_save_record()

        super().save(*args, **kwargs)

        if not was_changed:
            return

        if old_state.get('user_id') != self.user_id:
            self._handle_user_changed(old_state.get('user_id'))

        old_status = old_state.get('status', '')
        if old_status == self.status:
            return

        self.log.info('Membership %r changed status %r to %r',
                      self.pk, old_status, self.status)
        try:
            if self.status == 'active':
                signals.membership_activated.send(sender=self, old_status=old_status)
            elif old_status == 'active':
                signals.membership_deactivated.send(sender=self, old_status=old_status)
        except Exception:
            self.log.exception('Exception while sending membership state change signal '
                               'for membership %r, status=%r, old_status=%r',
                               self, self.status, old_status)
        return

    def template_sort(self) -> typing.Any:
        """Sorting key for listing memberships in templates."""
        try:
            status_idx = float(self.STATUSES.index(self.status))
        except ValueError:
            status_idx = float('inf')
        return status_idx, self.created_at, self.display_name

    def cancel(self) -> None:
        """Cancel the membership by cancelling the subscription."""
        from . import signals

        subscription = self.subscription
        if not subscription:
            signals.membership_cancelled.send(sender=self)
            # Without a subscription we have to do the work ourselves.
            self.log.info('Cancelling membership pk=%d without subscription; just deactivating',
                          self.pk)
            self.status = 'inactive'
            self.save(update_fields={'status'})
            return

        # With a subscription the cancellation of the subscription will
        # trigger the deactivation of the membership.
        self.log.info('Cancelling subscription pk=%d in order to cancel membership pk=%d',
                      subscription.pk, self.pk)
        try:
            subscription.cancel_subscription()
        except looper.exceptions.IncorrectStatusError as ex:
            self.log.info('Unable to cancel subscription pk=%d (status=%r) in order to cancel '
                          'membership pk=%d; going to deactivate membership.', subscription.pk,
                          subscription.status, self.pk)
            signals.membership_cancelled.send(sender=self)
            self.status = 'inactive'
            self.save(update_fields={'status'})
            return

        # Only send if we actually were deactivated because of the cancellation.
        # This doesn't happen when the subscription goes to 'pending-cancellation'
        if self.status == 'inactive':
            signals.membership_cancelled.send(sender=self)

    def get_absolute_url(self) -> str:
        return reverse('settings_membership_edit', kwargs={'membership_id': self.pk})

    def _handle_user_changed(self, old_user_id: typing.Optional[int]) -> None:
        """Change the user of connected objects too."""
        if old_user_id is None:
            return
        self.log.info('Membership %d changed user from %d to %s', self.pk, old_user_id, self.user)
        if not self.subscription:
            return
        self.subscription.user_id = self.user_id
        self.subscription.save(update_fields={'user_id'})

    def limit_fields(self) -> None:
        """Set all fields not allowed by the membership level to None.

        Only use this when sending the Membership to the front end, to hide
        fields that shouldn't even be set for this membership level.
        """

        for attr in self.level.hidden_attributes_set:
            setattr(self, attr, None)


class BadgerQueuedCall(models.Model):
    """Queued call to the Badger API of MyKDE.

    Ordinarily the API is called immediately, but when that fails the
    POST request is stored in this queue.
    """
    ACTIONS = [
        ('grant', 'Grant'),
        ('revoke', 'Revoke'),
    ]

    # These fields combine into the URL to call. This URL isn't persisted in
    # the database so that we can change the MyKDE URL in case it failed,
    # and queued calls will be done to the new MyKDE address instead of
    # the failing one.
    action = models.CharField(max_length=6, choices=ACTIONS)
    badge = models.CharField(max_length=32, help_text='The badge to grant/revoke')
    user = models.ForeignKey(User, help_text='The owner of the badge', null=True,
                             on_delete=models.CASCADE)

    error_code = models.IntegerField(blank=True, default=0,
                                     help_text='The HTTP status code received when POSTing')
    error_msg = models.TextField(blank=True, default='',
                                 help_text='The HTTP response received when POSTing')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    last_delivery_attempt = models.DateTimeField(null=True, blank=True, db_index=True)

    class Meta:
        verbose_name = 'Badger Queued Call'
        verbose_name_plural = 'Badger Queue'

    @property
    def url(self) -> str:
        """Construct the URL to POST to."""
        from urllib.parse import urljoin, quote

        bid_url = settings.BLENDER_ID['BASE_URL']
        badge = quote(self.badge)

        try:
            match_on = quote(str(self.user.oauth_info.oauth_user_id))
        except blender_id_oauth_client.models.OAuthUserInfo.DoesNotExist:
            log.warning('Badger matching on email address for OAuth-less user %r', self.user.email)
            match_on = quote(self.user.email)

        return urljoin(bid_url, f'api/badger/{self.action}/{badge}/{match_on}')
