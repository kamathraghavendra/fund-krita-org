from django import template

from blender_fund_main import models

register = template.Library()


@register.inclusion_tag('blender_fund/tags/membership_credit.html')
def membership_credit(membership: models.Membership):
    """Show a membership credit, used on the front page."""
    return {'membership': membership}
