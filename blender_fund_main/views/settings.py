import datetime
import logging
import pathlib
import typing

import django.forms
import django.utils.timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import FormView, TemplateView

from looper.models import Subscription, Order, Transaction
import looper.exceptions
import looper.utils
import looper.views.checkout
from looper.money import Money

from ..models import Membership
from .. import forms

log = logging.getLogger(__name__)

RECHARGEABLE_SUBS_STATUSES = {'active', 'on-hold'}


@login_required
def settings_home(request):
    mems = request.user.memberships.select_related('subscription', 'level').all()
    if len(mems) == 1:
        return redirect('settings_membership_edit', membership_id=mems[0].pk)
    mems = sorted(mems, key=Membership.template_sort)
    context = {'memberships': mems}
    return render(request, 'settings/home.html', context=context)


class SingleMembershipMixin(LoginRequiredMixin):
    @property
    def membership_id(self) -> int:
        return self.kwargs['membership_id']

    def get_membership(self) -> Membership:
        return get_object_or_404(
            self.request.user.memberships,
            pk=self.membership_id)

    def get_context_data(self, **kwargs) -> dict:
        mem = self.get_membership()
        subs: typing.Optional[Subscription] = mem.subscription

        return {
            **super().get_context_data(**kwargs),
            'membership': mem,
            'subscription': subs,
        }

    def dispatch(self, request, *args, **kwargs):
        if not hasattr(request.user, 'memberships'):
            # The AnonymousUser instance doesn't have a 'memberships' property,
            # but login checking only happens in the super().dispatch() call.
            return self.handle_no_permission()
        response = self.pre_dispatch(request, *args, **kwargs)
        if response:
            return response
        return super().dispatch(request, *args, **kwargs)

    def pre_dispatch(self, request, *args, **kwargs) -> typing.Optional[HttpResponse]:
        """Called between a permission check and calling super().dispatch().

        This allows you to get the current membership, or otherwise do things
        that require the user to be logged in.

        :return: None to continue handling this request, or a
            HttpResponse to stop processing early.
        """


class MembershipView(SingleMembershipMixin, FormView):
    form_class = forms.MembershipForm
    template_name = 'settings/membership_edit.html'

    membership: Membership
    fields_allowed_to_update: typing.Set[str]

    def pre_dispatch(self, request, *args, **kwargs):
        self.membership = self.get_membership()

        allowed_by_memlevel = self.membership.level.visible_attributes_set
        self.fields_allowed_to_update = allowed_by_memlevel | {'is_private'}

    def get_initial(self) -> dict:
        membership = self.membership
        initial = {k: getattr(membership, k)
                   for k in self.fields_allowed_to_update}
        return initial

    def get_form(self, form_class=None) -> forms.MembershipForm:
        form = super().get_form(form_class)
        membership = self.membership

        # If some fields should not be edited, simply show them as hidden.
        # The value will be ignored when saving, because of fields_allowed_to_update
        for attr in membership.level.hidden_attributes_set:
            form.fields[attr].widget = django.forms.HiddenInput()
            form.fields[attr].required = False
        return form

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        subs: typing.Optional[Subscription] = ctx['subscription']
        order: typing.Optional[Order] = None if not subs else subs.latest_order()
        trans: typing.Optional[Transaction] = None if not order else order.latest_transaction()

        # Whether there could be a charge (either manual or automatic)
        # for this subscription.
        may_be_charged = (subs is not None and
                          subs.status in RECHARGEABLE_SUBS_STATUSES and
                          subs.collection_method != 'managed')

        return {
            **ctx,
            'may_be_charged': may_be_charged,
            'order': order,
            'transaction': trans,
        }

    def form_valid(self, form):
        membership = self.get_membership()

        if membership.logo:
            old_logo = pathlib.Path(membership.logo.path)
        else:
            old_logo = None

        # TODO: move this into the Membership.save() method, after saving, to
        # keep the filesystem in sync with the database (even when the DB save fails).
        for allowed_field in self.fields_allowed_to_update & set(form.cleaned_data.keys()):
            if allowed_field == 'logo':
                self.update_logo(form, membership)
            else:
                setattr(membership, allowed_field, form.cleaned_data[allowed_field])

        # TODO(Sybren): flag this membership as being edited and posisbly requiring review.
        membership.save(update_fields=self.fields_allowed_to_update)

        self.maybe_delete_logo(membership, old_logo)

        return super().form_valid(form)

    def update_logo(self, form: forms.MembershipForm, membership: Membership) -> None:
        """Handle logo deletiong as well as uploads."""
        if form.cleaned_data['logo'] is None:
            # No new logo uploaded, but 'clear' checkbox also not checked.
            return

        if form.cleaned_data['logo'] is False:
            # We are deleting the image
            current_path = pathlib.Path(membership.logo.path)
            if current_path.is_file():
                current_path.unlink()
            membership.logo = None
        elif 'logo' in self.request.FILES:
            # We are uploading a new file
            membership.logo = self.request.FILES['logo']

    def maybe_delete_logo(self, membership: Membership, old_logo: typing.Optional[pathlib.Path]):
        """Delete the old logo if deleted or replaced by a new one."""

        if not old_logo or not old_logo.exists():
            return

        if membership.logo:
            current_path = pathlib.Path(membership.logo.path)
            if current_path == old_logo:
                return

        log.debug('Deleting old logo %s for membership %d', old_logo, membership.id)
        old_logo.unlink()

    def get_success_url(self) -> str:
        """Return the current URL."""
        return self.request.get_raw_uri()


class CancelMembershipView(SingleMembershipMixin, FormView):
    template_name = 'settings/membership_cancel.html'
    form_class = forms.CancelMembershipForm
    initial = {'confirm': False}

    _log = log.getChild('CancelMembershipView')

    def get_success_url(self) -> str:
        return reverse('settings_membership_edit',
                       kwargs={'membership_id': self.membership_id})

    def form_valid(self, form):
        membership = self.get_membership()
        self._log.info('Cancelling membership pk=%d on behalf of user pk=%d',
                       membership.pk, self.request.user.pk)
        membership.cancel()
        return super().form_valid(form)


class ExtendMembershipView(SingleMembershipMixin, looper.views.checkout.AbstractPaymentView):
    """Allow users to extend their membership by paying any amount."""
    # TODO(Sybren): maybe move this into Looper, or at least some of the code.
    # I put it here for now so that we have access to the membership without
    # jumping through any hoops.

    form_class = forms.FlexiblePaymentForm
    template_name = 'settings/extend_membership.html'

    membership: Membership
    subscription: Subscription
    price_per_month: Money

    def pre_dispatch(self, request, *args, **kwargs) -> typing.Optional[HttpResponse]:
        self.membership = self.get_membership()

        # Ensure that this view is only handled when there is a subscription.
        # Without subscription we have no next_payment date to shift.
        # Managed memberships can also not be extended in this manner.
        if not self.membership.subscription_id or self.membership.is_managed:
            return redirect('membership_extend_not_possible', membership_id=self.membership_id)

        self.subscription = self.membership.subscription
        self.price_per_month = self.membership.level.price_per_month(self.subscription.currency)
        self.gateway = self.subscription.payment_method.gateway

        return None

    def get_context_data(self, **kwargs) -> dict:
        days_per_month = 365 / 12  # Average number of days in a month.
        return {
            **super().get_context_data(**kwargs),
            # For showing the user.
            'price_per_month': self.price_per_month,

            # For calculating the new next_payment date in JavaScript.
            'price_per_day': self.price_per_month // days_per_month,
        }

    def get_form(self, form_class=None) -> forms.FlexiblePaymentForm:
        form: forms.FlexiblePaymentForm = super().get_form(form_class)
        price_field = form.fields['price']
        price_field.label += f' Enter any amount in {self.price_per_month.currency_symbol}'
        price_field.widget.attrs['placeholder'] = self.price_per_month.decimals_string
        price_field.widget.attrs['min'] = '5.00'
        # Pre-select the first available gateway option
        gateway_field = form.fields['gateway']
        form.initial['gateway'] = gateway_field.queryset.first().name
        return form

    def get_initial(self) -> dict:
        return {
            **super().get_initial(),
            'currency': self.price_per_month.currency,
            'email': self.customer.billing_email,
            'full_name': self.customer.full_name,
        }

    def get_form_kwargs(self) -> dict:
        return {
            **super().get_form_kwargs(),
            'instance': self.customer.billing_address,
        }

    def current_next_payment(self) -> datetime.datetime:
        """The current 'next payment' of self.subscription, or 'now' if unknown.

        :return: either self.subscription.next_payment, or 'now' if that
            property is None.
        """
        next_payment: typing.Optional[datetime.datetime] = self.subscription.next_payment
        if next_payment is None:
            return django.utils.timezone.now()
        return next_payment

    def form_valid(self, form: forms.FlexiblePaymentForm) -> HttpResponse:
        gateway = self.gateway_from_form(form)

        nonce = form.cleaned_data['payment_method_nonce']
        try:
            payment_method = self.customer.payment_method_add(nonce, gateway)
        except looper.exceptions.GatewayError as e:
            self.log.info('Error adding payment method: %s', e.errors)
            form.add_error('', e.with_errors())
            return self.form_invalid(form)

        # Create a new order, or use the one from the form (in case we created one in an
        # earlier attempt).
        order_pk = form.cleaned_data.get('order_pk')
        if order_pk:
            order: looper.models.Order = get_object_or_404(self.subscription.order_set, pk=order_pk)
            self.log.debug('Reusing order pk=%r for checkout', order.pk)
        else:
            # Create an order with the correct payment method, as this may be a different one
            # than connected to the subscription.
            order = self.subscription.generate_order(save=False)
            order.payment_method = payment_method
            self.log.debug('Creating order pk=%r for checkout with payment method %r',
                           order.pk, payment_method.pk)

        # Make sure the order reflects whatever the user entered in the form, rather
        # than the subscription's price.
        order.price = form.cleaned_data['price']
        order.save()

        trans = order.generate_transaction()

        customer_ip_address = looper.utils.get_client_ip(self.request)
        if not trans.charge(customer_ip_address=customer_ip_address):
            form.add_error('', trans.failure_message)
            form.data = {
                **form.data.dict(),  # form.data is a QueryDict, which is immutable.
                'order_pk': order.pk,
            }
            return self.form_invalid(form)
        self.erase_client_token()

        months_bought = order.price / self.price_per_month
        assert isinstance(months_bought, float)
        self.log.debug('User bought %.3f months for %s (per month is %s)',
                       months_bought, order.price, self.price_per_month)
        self.subscription.extend_subscription(from_timestamp=self.current_next_payment(),
                                              months=months_bought)

        return redirect('membership_extend_done', membership_id=self.membership.id)


class ExtendMembershipNotPossibleView(SingleMembershipMixin, TemplateView):
    template_name = 'settings/extend_membership_not_possible.html'


class ExtendMembershipDoneView(SingleMembershipMixin, TemplateView):
    template_name = 'settings/extend_membership_done.html'
