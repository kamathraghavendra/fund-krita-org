
/home/blender-fund                           Home directory of user blender-fund
/var/www/blender-fund                        Django project
/var/www/settings/blender_fund_settings.py   Django production configuration

/var/lib/mysql        MySQL database
/var/spool/postfix    Postfix spool directory (mail queue)
/var/log              Most of the logs
